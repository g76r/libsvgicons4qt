TEMPLATE=aux
custom.commands = $(MAKE) -f custom.mk
customclean.commands = $(MAKE) -f custom.mk clean
distclean.depends = customclean
QMAKE_EXTRA_TARGETS += custom customclean distclean
PRE_TARGETDEPS += custom
