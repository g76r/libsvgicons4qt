TARGETS=svgicons4qt.qrc fas.qrc far.qrc fab.qrc im.qrc im-d.qrc mat.qrc typ.qrc

.PHONY: all clean distclean re default

default: all

.SUFFIXES: .qrc

%.qrc: %
	./buildqrc $@ $^

svgicons4qt.qrc: fas far fab im im-d mat typ
	./buildqrc $@ $^

all: $(TARGETS)

clean:
	rm -f $(TARGETS)

distclean: clean

re: clean all
