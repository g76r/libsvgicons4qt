SVG icons are derived works from widespread free icons fonts and are made
available as such under different licences depending on their origin (and
actually on their original authors licences choices):

* Font-Awesome ("fa*" directories)
  License: SIL-OFL 1.1
  Credits: Dave Gandy (http://fortawesome.github.com/Font-Awesome/)

* IcoMoon Free ("im" and "im-d" directories) 
  License: GPL 3+ or CC-BY 4.0
  Credits: Keyamoon (http://keyamoon.com)

* Material ("mat" and "mat-d" directories)
  License: CC-BY 4.0
  Credits: Google (https://www.google.com/design/icons/)

* Typicons ("typ" and "typ-d" directories)
  License: SIL-OFL 1.1
  Credits: Stephen Hutchings (http://www.s-ings.com/)

Other files (such as resources shell scripts or build files) are provided
under CC0 1.0 license terms.

Licences full legal text can be found in "licenses" directory.
