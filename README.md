libsvgicons4qt
==============

This is a library budling widespread high-quality icons fonts as SVG files
with convenient resources files to use them in Qt GUI applications.

Currently these icon fonts have been used to produce SVG files:
* Font Awesome by Dave Gandy (http://fortawesome.github.com/Font-Awesome/)
* Icomoon by Keyamoon (http://keyamoon.com)
* Material Icons by Google (https://www.google.com/design/icons/)
* Typicons (http://www.s-ings.com/)

A few modified/derived icons are also provided (in "*-d" directories).

licenses
========

Icons are provided under several different usage licenses, depending on
their origin. See COPYING.md file.
